package sandeep.com.test.model.cache;


import android.util.LruCache;

import sandeep.com.test.model.feed.ListFeed;

public class ListFeedCache extends LruCache<String, ListFeed> {

    private static final String LIST_FEED_KEY = "lfk";

    private static ListFeedCache instance;

    private ListFeedCache() {
        super(1);
    }

    public static ListFeedCache getInstance() {
        if (instance == null) instance = new ListFeedCache();
        return instance;
    }

    public ListFeed get() {
        return super.get(LIST_FEED_KEY);
    }

    public void put(ListFeed value) {
        super.put(LIST_FEED_KEY, value);
    }
}
