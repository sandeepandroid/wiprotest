package sandeep.com.test.model.repo;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import sandeep.com.test.model.feed.BaseFeed;

public abstract class BaseRepository<K extends BaseFeed> {

    protected static Executor executor = Executors.newFixedThreadPool(2);

    protected abstract void fetchApi(String url, Callback<K> callback, boolean isFromPull);

    public void fetch(final String url, final Callback<K> callback, boolean isFromPull) {
        fetchApi(url, callback, isFromPull);
    }

    public interface Callback<K> {
        void onSuccess(K data);

        void onFail(Throwable throwable);
    }


}
