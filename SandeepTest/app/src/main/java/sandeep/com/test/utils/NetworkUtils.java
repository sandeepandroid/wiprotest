package sandeep.com.test.utils;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import sandeep.com.test.WiproApplication;

public class NetworkUtils {
    public static boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) WiproApplication.getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
