package sandeep.com.test.view;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import sandeep.com.test.R;


public class ImageDataBindingAdapter {

    @BindingAdapter({"image_url"})
    public static void loadImageWithType(ImageView imageView, String url) {
        Context context = imageView.getContext();
        RequestOptions requestOptions = RequestOptions.placeholderOf(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher);
        Glide.with(context).applyDefaultRequestOptions(requestOptions).load(url).into(imageView);
    }


}
