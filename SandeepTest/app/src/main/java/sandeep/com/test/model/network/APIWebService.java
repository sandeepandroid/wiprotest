package sandeep.com.test.model.network;

public class APIWebService {
    private static APIInterface webService = null;

    public static APIInterface getPrimeApiService() {
        if (webService == null) {
            webService = APIClient.getClient().create(APIInterface.class);
        }
        return webService;
    }

}
