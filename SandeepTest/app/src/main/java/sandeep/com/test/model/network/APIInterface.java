package sandeep.com.test.model.network;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;
import sandeep.com.test.model.feed.ListFeed;


public interface APIInterface {

    @GET
    Call<ListFeed> getHomeData(@Url String url);

}
