package sandeep.com.test.viewmodel;

import android.arch.lifecycle.MutableLiveData;

import sandeep.com.test.model.feed.ListFeed;
import sandeep.com.test.model.repo.BaseRepository;
import sandeep.com.test.model.repo.ListRepository;


public class ListViewModel extends BaseViewModel<ListFeed> {
    @Override
    protected void fetchApi(final String apiUrl, final MutableLiveData<ViewModelDto<ListFeed>> status, boolean b) {

        new ListRepository().fetch(apiUrl, new BaseRepository.Callback<ListFeed>() {
            @Override
            public void onSuccess(ListFeed data) {
                status.setValue(new ViewModelDto(Status.PASS, apiUrl, data, null));

            }

            @Override
            public void onFail(Throwable throwable) {
                status.setValue(new ViewModelDto(Status.FAIL, apiUrl, null, throwable));

            }
        }, b);
    }

}
