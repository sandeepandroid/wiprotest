package sandeep.com.test.model.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;


import sandeep.com.test.model.feed.ListFeed;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface ListFeedDao {

    @Insert(onConflict = REPLACE)
    void save(ListFeed body);

    @Query("SELECT * FROM ListFeed")
    ListFeed fetch();
}
