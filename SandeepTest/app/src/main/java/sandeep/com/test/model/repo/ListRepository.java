package sandeep.com.test.model.repo;


import android.arch.persistence.room.Room;
import android.content.Context;
import android.os.Handler;

import java.io.IOException;

import retrofit2.Response;
import sandeep.com.test.WiproApplication;
import sandeep.com.test.model.cache.ListFeedCache;
import sandeep.com.test.model.dao.AppDb;
import sandeep.com.test.model.feed.ListFeed;
import sandeep.com.test.model.network.APIWebService;
import sandeep.com.test.utils.NetworkUtils;

public class ListRepository extends BaseRepository<ListFeed> {
    private static final String LIST_FEED_PREF = "lfp";
    private static final String PREF_KEY_CHECK_SYNC = "rkchs";
    private static final long CHECK_STALE_TIME = 10 * 60;// 10 mins
    private final AppDb db;
    private Handler uiHandler;

    public ListRepository() {
        db = Room.databaseBuilder(WiproApplication.getAppContext(), AppDb.class, "database-name").build();
        uiHandler = new Handler();
    }

    @Override
    protected void fetchApi(String url, final Callback<ListFeed> callback, boolean isFromPull) {
        executor.execute(() -> {
            if (isFromPull) {
                fetchFromNetwork(url, callback);
                return;
            }
            if (ListFeedCache.getInstance().get() != null) {
                uiHandler.post(() -> callback.onSuccess(ListFeedCache.getInstance().get()));
                return;
            }
            if (NetworkUtils.isOnline()) {
                if (isRootFeedExistLocally() && !isRootExpired()) {
                    fetchFromLocal(callback);
                } else {
                    fetchFromNetwork(url, callback);
                }
            } else {
                fetchFromLocal(callback);
            }
        });


    }


    private void fetchFromLocal(final Callback<ListFeed> callback) {
        ListFeed rootFeed = db.getListFeedDao().fetch();
        if (rootFeed != null)
            ListFeedCache.getInstance().put(rootFeed);
        uiHandler.post(() -> callback.onSuccess(rootFeed));

    }

    private void fetchStaleRoot(String error, Callback<ListFeed> callback) {
        if (!isRootFeedExistLocally() || isRootExpired()) {
            uiHandler.post(() -> callback.onFail(new Throwable(error)));
        } else {
            ListFeed fetch = db.getListFeedDao().fetch();
            ListFeedCache.getInstance().put(fetch);
            uiHandler.post(() -> callback.onSuccess(fetch));
        }
    }

    private void fetchFromNetwork(String url, Callback<ListFeed> callback) {
        try {
            Response<ListFeed> response = APIWebService.getPrimeApiService().getHomeData(url).execute();
            if (response.isSuccessful()) {
                ListFeedCache.getInstance().put(response.body());
                db.getListFeedDao().save(response.body());
                saveFeedSyncTime();
                if (callback != null) {
                    uiHandler.post(() -> callback.onSuccess(response.body()));

                }
            } else {
                if (isRootFeedExistLocally()) {
                    ListFeed fetch = db.getListFeedDao().fetch();
                    ListFeedCache.getInstance().put(fetch);
                    uiHandler.post(() -> callback.onSuccess(fetch));
                } else {
                    fetchStaleRoot(response.errorBody().toString(), callback);
                }
            }
        } catch (IOException e) {


        }

    }


    private void saveFeedSyncTime() {
        WiproApplication.getAppContext().getSharedPreferences(LIST_FEED_PREF, Context.MODE_PRIVATE).edit().putInt(PREF_KEY_CHECK_SYNC, (int) (System.currentTimeMillis() / 1000)).apply();
    }

    private boolean isRootExpired() {
        return System.currentTimeMillis() / 1000 - WiproApplication.getAppContext().getSharedPreferences(LIST_FEED_PREF, Context.MODE_PRIVATE).getInt(PREF_KEY_CHECK_SYNC, -1) > CHECK_STALE_TIME;
    }

    private boolean isRootFeedExistLocally() {
        return WiproApplication.getAppContext().getSharedPreferences(LIST_FEED_PREF, Context.MODE_PRIVATE).contains(PREF_KEY_CHECK_SYNC);
    }
}
