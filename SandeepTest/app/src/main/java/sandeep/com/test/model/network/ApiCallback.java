package sandeep.com.test.model.network;

import java.net.HttpURLConnection;

import retrofit2.Call;
import retrofit2.Response;

public abstract class ApiCallback<K> implements retrofit2.Callback<K> {

    @Override
    public void onResponse(Call<K> call, Response<K> response) {
        if (response.isSuccessful() && response.code() == HttpURLConnection.HTTP_OK) {
            onSuccess(call, response);
        } else if (response.isSuccessful() && response.code() == HttpURLConnection.HTTP_NOT_MODIFIED) {
            onUnModifiedSuccess(call, response);
        }
    }


    @Override
    public void onFailure(Call<K> call, Throwable t) {
        onFail(call, t);
    }


    public abstract void onSuccess(Call<K> call, Response<K> response);

    public abstract void onUnModifiedSuccess(Call<K> call, Response<K> response);

    public abstract void onFail(Call<K> call, Throwable t);
}
