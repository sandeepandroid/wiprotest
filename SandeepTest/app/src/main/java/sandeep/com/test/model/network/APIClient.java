package sandeep.com.test.model.network;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {

    public static final String BASE_URL = "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/";

    private static Retrofit retrofitClient;


    public static Retrofit getClient() {

        if (retrofitClient == null) {
            retrofitClient = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(getHTTPClient())  // Used to set header using OkHttpClient.
                    .addConverterFactory(GsonConverterFactory.create())//Used to convert the JSON response into java Objects.
                    .build();
        }
        return retrofitClient;
    }

    private static OkHttpClient getHTTPClient() {
        final OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient.connectTimeout(30000, TimeUnit.MILLISECONDS);
        httpClient.readTimeout(30000, TimeUnit.MILLISECONDS);
        httpClient.writeTimeout(30000, TimeUnit.MILLISECONDS);

        return httpClient.build();

    }


}
