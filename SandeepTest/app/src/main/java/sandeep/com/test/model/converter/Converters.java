package sandeep.com.test.model.converter;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import sandeep.com.test.model.feed.Row;

public class Converters {
    @TypeConverter
    public static ArrayList<Row> fromString(String value) {
        Type listType = new TypeToken<ArrayList<Row>>() {}.getType();
        return new Gson().fromJson(value, listType);
    }
 
    @TypeConverter
    public static String fromArrayLisr(ArrayList<Row> list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);
        return json;
    }
}