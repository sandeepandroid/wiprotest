package sandeep.com.test.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import sandeep.com.test.model.feed.BaseFeed;


public abstract class BaseViewModel<K extends BaseFeed> extends ViewModel {

    protected final MutableLiveData<ViewModelDto<K>> status;

    public BaseViewModel() {
        status = new MutableLiveData<>();
    }

    public void fetch(String apiUrl,boolean isFromPull) {

        status.setValue(new ViewModelDto(Status.INIT, apiUrl,null, null));

        fetchApi(apiUrl, status,isFromPull);
    }


    protected abstract void fetchApi(String apiUrl, final MutableLiveData<ViewModelDto<K>> status,boolean isFromPull);

//
    public MutableLiveData<ViewModelDto<K>> getLiveData() {
        return status;

    }

    public interface Status {
        int INIT = 666;
        int PASS = 667;
        int FAIL = 668;
    }

    public static class ViewModelDto<K extends BaseFeed> {

        protected int status;

        protected K data;

        protected Throwable error;

        protected String url;

        public ViewModelDto(int status, String apiUrl, K data, Throwable error) {
            this.status = status;
            this.data = data;
            this.error = error;
            this.url = apiUrl;
        }

        public String getUrl(){
            return url;
        }

        public int getStatus() {
            return status;
        }

        public K getData() {
            return data;
        }

        public Throwable getError() {
            return error;
        }
    }
}
