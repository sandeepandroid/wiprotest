package sandeep.com.test;

import android.app.Application;
import android.content.Context;

import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.MemoryCategory;

public class WiproApplication extends Application {
    public static Context getAppContext() {
        return mInstance.getApplicationContext();
    }

    private static WiproApplication mInstance;

    public WiproApplication() {
        mInstance = this;
    }

    public static WiproApplication getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Glide.get(this).setMemoryCategory(MemoryCategory.HIGH);
    }
}
