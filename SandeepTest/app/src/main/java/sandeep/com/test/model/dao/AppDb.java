package sandeep.com.test.model.dao;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import sandeep.com.test.model.feed.ListFeed;


@Database(entities = {ListFeed.class}, version = 1, exportSchema = false)

public abstract class AppDb extends RoomDatabase {
    public abstract ListFeedDao getListFeedDao();


}

