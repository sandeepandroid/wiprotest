package sandeep.com.test.activities;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;

import sandeep.com.test.BR;
import sandeep.com.test.R;
import sandeep.com.test.databinding.ActivityMainBinding;
import sandeep.com.test.model.feed.Row;
import sandeep.com.test.utils.NetworkUtils;
import sandeep.com.test.utils.RetryHandler;
import sandeep.com.test.viewmodel.BaseViewModel;
import sandeep.com.test.viewmodel.ListViewModel;

public class MainActivity extends AppCompatActivity implements RetryHandler {

    private ListViewModel listViewModel;
    private ActivityMainBinding binding;
    private String title;
    private ArrayList<Row> rows = new ArrayList<>();
    private SwipeRefreshLayout swipeLayout;
    private RecyclerView rv;
    private MyAdapter myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        listViewModel = ViewModelProviders.of(this).get(ListViewModel.class);
        binding.setVariable(BR.fetchStatus, 0);
        binding.setVariable(BR.retryHandler, this);
        binding.executePendingBindings();

        swipeLayout = binding.swipeLayout;
        swipeLayout.setOnRefreshListener(this::fetchFreshData);

        rv = binding.rv;
        rv.setItemViewCacheSize(20);
        rv.setDrawingCacheEnabled(true);
        rv.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        //DividerItemDecoration itemDecor = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        //rv.addItemDecoration(itemDecor);
        rv.setLayoutManager(new LinearLayoutManager(this));
        myAdapter = new MyAdapter();
        rv.setAdapter(myAdapter);

//        if (!NetworkUtils.isOnline()) {
//            binding.setVariable(BR.errorString, "No internet connection");
//            binding.setVariable(BR.fetchStatus, 2);
//            binding.executePendingBindings();
//            return;
//        }
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (savedInstanceState != null) {
            title = savedInstanceState.getString("title");
            rows = savedInstanceState.getParcelableArrayList("rows");
            showUi();
            return;
        }


        listViewModel.fetch("facts.json/",false);
        listViewModel.getLiveData().observe(this, listFeedViewModelDto -> {
            if (listFeedViewModelDto != null) {
                switch (listFeedViewModelDto.getStatus()) {
                    case BaseViewModel.Status.PASS:
                        if (listFeedViewModelDto.getData() != null) {
                            title = listFeedViewModelDto.getData().getTitle();
                            ArrayList<Row> list = listFeedViewModelDto.getData().getRows();
                            if (list != null && !list.isEmpty()) {
                                filterList(list);
                            }
                        }else{
                            if (!NetworkUtils.isOnline()) {
                                binding.setVariable(BR.errorString, "No internet connection");
                                binding.setVariable(BR.fetchStatus, 2);
                                binding.executePendingBindings();
                                return;
                            }else{
                                binding.setVariable(BR.errorString, "No cached data");
                                binding.setVariable(BR.fetchStatus, 2);
                                binding.executePendingBindings();
                            }
                        }
                        binding.executePendingBindings();
                        break;
                    case BaseViewModel.Status.FAIL:
                        binding.setVariable(BR.errorString, "Problem occur. ");
                        binding.setVariable(BR.fetchStatus, 2);
                        binding.executePendingBindings();
                        break;
                }
            }
        });

    }

    private void filterList(ArrayList<Row> list) {
        if (!rows.isEmpty()) {
            rows.clear();
        }
        for (int i = 0; i < list.size(); i++) {
            Row row = list.get(i);
            if (!TextUtils.isEmpty(row.getDescription()) && !TextUtils.isEmpty(row.getTitle())
                    && !TextUtils.isEmpty(row.getImageHref())) {
                rows.add(row);
            }
        }
        showUi();
    }

    private void fetchFreshData() {
        if (!NetworkUtils.isOnline()) {
            swipeLayout.setRefreshing(false);
            Toast.makeText(this, "no internet connection", Toast.LENGTH_SHORT).show();
            return;
        }
        listViewModel.fetch("facts.json/",true);
        listViewModel.getLiveData().observe(this, listFeedViewModelDto -> {
            if (listFeedViewModelDto != null) {
                swipeLayout.setRefreshing(false);
                switch (listFeedViewModelDto.getStatus()) {
                    case BaseViewModel.Status.PASS:
                        if (listFeedViewModelDto.getData() != null) {
                            title = listFeedViewModelDto.getData().getTitle();
                            ArrayList<Row> list = listFeedViewModelDto.getData().getRows();
                            if (list != null && !list.isEmpty()) {
                                filterList(list);
                            }
                        }
                        binding.executePendingBindings();
                        break;
                    case BaseViewModel.Status.FAIL:

                        break;
                }
            }
        });
    }

    private void showUi() {
        if (rows == null) {
            binding.setVariable(BR.errorString, "No data available ");
            binding.setVariable(BR.fetchStatus, 2);
            binding.executePendingBindings();
            return;
        }
        binding.setVariable(BR.fetchStatus, 1);
        getSupportActionBar().setTitle(title);
        myAdapter.update(rows);
    }

    @Override
    public void onRetry() {
        if (!NetworkUtils.isOnline()) {
            binding.setVariable(BR.errorString, "No internet connection");
            binding.setVariable(BR.fetchStatus, 2);
            binding.executePendingBindings();
            return;
        }
        binding.setVariable(BR.fetchStatus, 0);
        binding.executePendingBindings();

        fetchFreshData();
    }

    public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
        private ArrayList<Row> rowData;


        public void update(ArrayList<Row> rowData) {
            this.rowData = rowData;
            notifyDataSetChanged();
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            private final ViewDataBinding binding;

            ViewHolder(ViewDataBinding binding) {
                super(binding.getRoot());
                this.binding = binding;
            }

            void bindSmallNewsView(Row newsItems) {
                binding.setVariable(BR.newsItem, newsItems);
                binding.executePendingBindings();
            }
        }


        @Override
        public MyAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            int layoutResId = R.layout.list_item_todays_news_small;

            ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), layoutResId, parent, false);
            return new ViewHolder(binding);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            (holder).bindSmallNewsView(rowData.get(position));

        }

        @Override
        public int getItemCount() {
            return rowData.size();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("rows", rows);
        outState.putString("title", title);
    }

}
