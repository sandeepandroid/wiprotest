package sandeep.com.test;


import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.ArrayList;

import sandeep.com.test.model.feed.ListFeed;
import sandeep.com.test.model.feed.Row;

public class UnitTest {

    @Mock
    private ListFeed mock;


    @Test
    public void createMock() throws Exception {
        mock = Mockito.mock(ListFeed.class);
    }

    /*for empty list*/
    @Test
    public void homePageListTestEmpty() {
        ListFeed mock = Mockito.mock(ListFeed.class);
        Mockito.when(mock.getRows()).thenReturn(new ArrayList<Row>());
        Mockito.verify(mock);
    }

    /*for list with only one item*/
    @Test
    public void homePageListTestWithData() {
        ListFeed mock = Mockito.mock(ListFeed.class);
        ArrayList<Row> list = new ArrayList<>();
        Row row = new Row();
        row.setDescription("sadmagdmajs");
        row.setTitle("asdjkhasdj");
        list.add(row);
        Mockito.when(mock.getRows()).thenReturn(list);
    }

    /*list for null*/
    @Test
    public void homePageListTestWithNull() {
        ListFeed mock = Mockito.mock(ListFeed.class);
        Mockito.when(mock.getRows()).thenReturn(null);
        Mockito.verify(mock);
    }




    /*page title  test*/
    @Test
    public void homePageTitle() {
        ListFeed mock = Mockito.mock(ListFeed.class);
        String title = mock.getTitle();

        Mockito.verify(mock).getTitle();
        Mockito.verifyNoMoreInteractions(mock);
    }

    /*page title for null test*/
    @Test
    public void homePageTitleNull() {
        ListFeed mock = Mockito.mock(ListFeed.class);
        String title = mock.getTitle();
        Mockito.when(title).thenReturn(null);
    }

    /*page title for Empty test*/
    @Test
    public void homePageTitleEmpty() {
        ListFeed mock = Mockito.mock(ListFeed.class);
        Mockito.when(mock.getTitle()).thenReturn("");
    }
}