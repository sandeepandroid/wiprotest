package sandeep.com.test;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.content.Context;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoRule;

import okhttp3.Response;
import sandeep.com.test.model.repo.BaseRepository;
import sandeep.com.test.model.repo.ListRepository;
import sandeep.com.test.viewmodel.ListViewModel;

public class UnitTest2 {

    @Mock
    private ListRepository repository;
    @Mock
    private BaseRepository.Callback viewContract;

    private ListViewModel listViewModel;

    @Rule
    public InstantTaskExecutorRule rule = new InstantTaskExecutorRule();


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);// required for the "@Mock" annotations

        // Make presenter a mock while using mock repository and viewContract created above
        Mockito.when(WiproApplication.getAppContext()).thenReturn(Mockito.mock(Context.class));
        listViewModel = Mockito.spy(new ListViewModel());

    }

    @Test
    public void searchGitHubRepos_noQuery() {
        Context spy = Mockito.spy(Context.class);
        Mockito.when(WiproApplication.getAppContext()).thenReturn(spy);
        String searchQuery = null;
        // Trigger
        listViewModel.fetch(searchQuery,false);
        // Validation
        Mockito.verify(repository, Mockito.never()).fetch(searchQuery, viewContract,false);
    }

    @Test
    public void searchGitHubRepos() {
        String searchQuery = "some query";
        // Trigger
        listViewModel.fetch(searchQuery,false);
        // Validation
        Mockito.verify(repository, Mockito.times(1)).fetch(searchQuery, viewContract,false);
    }


//    @SuppressWarnings("unchecked")
//    @Test
//    public void handleGitHubResponse_Success() {
//        Response response = Mockito.mock(Response.class);
//        SearchResponse searchResponse = Mockito.mock(SearchResponse.class);
//        Mockito.doReturn(true).when(response).isSuccessful();
//        Mockito.doReturn(searchResponse).when(response).body();
//        List<SearchResult> searchResults = new ArrayList<>();
//        searchResults.add(new SearchResult());
//        searchResults.add(new SearchResult());
//        searchResults.add(new SearchResult());
//        Mockito.doReturn(searchResults).when(searchResponse).getSearchResults();
//        // trigger
//        presenter.handleGitHubResponse(response);
//        // validation
//        Mockito.verify(viewContract, Mockito.times(1)).displaySearchResults(searchResults);
//    }
//
//    @Test
//    public void handleGitHubResponse_Failure() {
//        Response response = Mockito.mock(Response.class);
//
//        // case No. 1
//        Mockito.doReturn(false).when(response).isSuccessful();
//        // trigger
//        presenter.handleGitHubResponse(response);
//        // validation
//        Mockito.verify(viewContract, Mockito.times(1)).displayError(Mockito.anyString());
//        /*
//        if the following is not called, viewContract.displayError() will be
//        mistakenly considered as invoked twice
//         */
//        Mockito.reset(viewContract);
//
//        // case No. 2
//        Mockito.doReturn(null).when(response).body();
//        // trigger
//        presenter.handleGitHubResponse(response);
//        // validation
//        Mockito.verify(viewContract, Mockito.times(1)).displayError(Mockito.anyString());
//    }
//
//    @Test
//    public void handleGitHubError() {
//        // trigger
//        presenter.handleGitHubError();
//        // validation
//        Mockito.verify(viewContract, Mockito.times(1)).displayError();
//    }
}